package com.hs.hsbaseweb.controller;

import com.hs.hsbaseweb.entity.EmployeeEntity;
import com.hs.hsbaseweb.jpa.EmployeeJpa;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {
    //日志打印
    private final static Logger logger=LoggerFactory.getLogger(EmployeeController.class);
    @Autowired
    EmployeeJpa employeeJpa;

    @RequestMapping(value = "/findAll",method = RequestMethod.GET)
    public List<EmployeeEntity> employeeEntityList(){
        logger.debug("debug日志...");
        logger.info("info日志...");
        logger.error("error日志...");

        EmployeeEntity emplist=employeeJpa.findById("1").get();
        emplist.setName("caij");
        employeeJpa.save(emplist);

        return employeeJpa.findAll();
    }


}
