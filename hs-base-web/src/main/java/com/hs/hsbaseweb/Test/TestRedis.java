package com.hs.hsbaseweb.Test;

import com.hs.hsbaseweb.entity.User;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Redis 测试类
 */
@RestController
@RequestMapping("/user")
public class TestRedis {

    @Cacheable(value="user-key")
    @GetMapping("/findAll")
    public User getUser() {
        User user=new User("aa@126.com", "aa", "aa123456");
        System.out.println("若下面没出现“无缓存的时候调用”字样且能打印出数据表示测试成功");
        return user;
    }
}
