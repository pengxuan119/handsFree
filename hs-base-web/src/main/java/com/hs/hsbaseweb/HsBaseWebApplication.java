package com.hs.hsbaseweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HsBaseWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(HsBaseWebApplication.class, args);
    }

}
