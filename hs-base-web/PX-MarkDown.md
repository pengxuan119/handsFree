# ImageIO.read()无法读取CMYK

```html
报错信息：Unsupported Image Type
解决方案:添加maven文件即可
<!-- cmyk格式图片转换 -->
        <dependency>
            <groupId>com.twelvemonkeys.imageio</groupId>
            <artifactId>imageio-jpeg</artifactId>
            <version>3.3</version>
        </dependency>
        <dependency>
            <groupId>com.twelvemonkeys.imageio</groupId>
            <artifactId>imageio-tiff</artifactId>
            <version>3.3</version>
        </dependency>
```

# 反编译软件使用及JDK配置（Win10）

```
系统变量->JAVA_HOME-> C:\jdk1.8.0_144
系统变量->CLASSPATH-> .;%JAVA_HOME%\lib;%JAVA_HOME%\lib\tools.jar;
系统变量->Path------> %JAVA_HOME%\bin->%JAVA_HOME%\jre\bin
启动反编译方式->java -jar .\jd-gui.exe
```

# gulp 打包报错

```shell
报错信息：：ReferenceError: internalBinding is not defined
问题原因:
这个问题是我将node版本升级至v10.15.0，npm升级至6.4.1后出现的，在此之前，我的node版本是8+，没有出现这个问题。
解决方案:
npm install natives@1.1.6
```



# AJAX_JSON前后台传输写法

## 整理1

```javascript
//js请求写法
var packGsonCode;
var index = viewModel.PacSpecSonData.getSelectedRows();
for (var i = 0; i < index.length; i++) {
       code.push(index[i].data.matchingCode.value);
}
packGsonCode = code.join(',');
$._ajax({
     url: appCtx + "/base/pac-spec-gsons/saveNumToSpecScale",
     type: "post",
     contentType: "application/json; charset=utf-8",
     data: JSON.stringify({
     		"brand": brand,
        "bigCategory": bigCategory,
        "orderBatch": orderBatch,
        "specSonId": specSonId,
        "packGsonCode": packGsonCode
      }),
     success: function (result) {
         if (result) {
             toastr.success("数据存入规格比例设置成功！");
          } else {
             toastr.error("数据存入规格比例设置失败！");
          }
     },
});
```

```java
//根据brand,bigCategory,orderBatch,specSonId,packGsonCode数据保存到规格比例设置里的nums
    @PostMapping("/saveNumToSpecScale")
    public boolean saveNumToSpecScale(@RequestBody SpecScaleJson spec) {
        String code[]=specScaleJson.getPackGsonCode().split(",");
        return service.saveNumToSpecScale(
                spec.getBrand(),spec.getBigCategory(),
                spec.getOrderBatch(),spec.getSpecSonId(),code);
    }
```

```java
//仓库数组写法
@Query(value = "SELECT * from BOOK_PACKAGE_VIEW_DETAIL WHERE BRAND=?1 AND FIRST_CATEGORY=?2 AND ORDER_BATCH=?3 AND SPEC_RANGE=?4 AND CODE in ?5",nativeQuery = true) 
List<PackageViewDetail> findAllBySearch(String brand, String bigCategory, String orderBatch, String specSonId, String[] packGsonCode);
```

## 整理2

```javascript
//前端写法
rounderUp: function () {
		var queryData = singledocSearch.getDataWithOpr();
    if (queryData!= null) {
         $._ajax({
             type: "Post",
             url: appCtx + viewModel.baseurl + "/rounded-up",
             dataType: "json",
             data: {
                "orderBatch": queryData.search_EQ_orderBatch,
                "firstCategory": queryData.search_EQ_firstCategory,
                "brand": queryData.search_EQ_brand
             },
             success: function (data) {
                if (data) {
                    toastr.success('向上尾箱调整成功,请点击确认完成');
                } else {
                     toastr.error('向上尾箱调整失败');
                }
             		viewModel.search();
              }
           });
    }
}
```

```java
//后端写法
@RequestMapping("/rounded-up")
public boolean roundedUp(@RequestParam("orderBatch") String orderBatch,@RequestParam("firstCategory") String firstCategory,@RequestParam("brand") String brand) {
		return service.roundedUp(orderBatch,firstCategory,brand);
} 
```

```json
//前台请求的数据格式（Chrome）
Form Data		view source view		URL encoded
	orderBatch:3654caf4-2db6-4ce6-a8db-686b73ea0977
	firstCategory:02
	brand:Y
```

# SQL语句

```mssql
//创建视图
create or replace view NAME_VIEW  as select * from table_1;

//WITH temp as
with tempName as (select * from table_1) 
select a.*,b.* from tempname a left table_2 b on (a.id=b.parent_id)
where b.dr=0;

//插入数据
INSERT INTO table_name(id,code) VALUES('xx','xx')

//增加/删除索引
create Index IDX_索引名 on 表名 (字段1,字段2);//索引名：表名+字段名，也可以是流水号。字段1和字段2可以是组合索引。
drop index 索引名;

//删除外键约束
alter table table_name disable primary key cascade;

//增加唯一约束
ALTER TABLE table_name ADD CONSTRAINT 约束名 UNIQUE (字段名);

//CASE WHEN
CASE
	WHEN b.id IS NULL THEN
	'0' ELSE '1' 
	END orderState,
	
//备份临时表数据
create table xxx_temp as select * from xxx;

//增加字段/删除字段
alter table tablename add col varchar2(50);
alter table tablename drop (column1,column2);

//查看DBLink
select * from dba_db_links;

//连接DBLink
select t.*  from table_name@b2c_link t;
 
//创建DBLink ,需要注意：创建DBLink的时候，需要在当前用户连接下创建，否则不能创建。
CREATE DATABASE LINK "B2C_LINK" CONNECT TO "OCC_B2C" IDENTIFIED BY
"密码" USING '(DESCRIPTION =(ADDRESS_LIST =(ADDRESS =(PROTOCOL = TCP)(HOST = IP地址)(PORT = 端口)))(CONNECT_DATA =(SERVICE_NAME = 服务名)))'

//删除DBLink
DROP DATABASE LINK "STOCK_LINK";

//创建表
CREATE TABLE table_name1 AS (SELECT * FROM	table_name2);

//时间转换
select TO_CHAR(SYSDATE, 'yyyy-MM-dd HH24:mi:ss' )  from dual;

//查询oracl所有表
SELECT
	t.table_name 
FROM
	user_col_comments t 
WHERE
	t.column_name = 'BRAND' 
	AND t.table_name NOT LIKE '%BOOK%' 
	AND t.table_name LIKE '%BASE%';
	
//Merge数据
MERGE INTO CLASS_PX a --目标表
USING STUDENT_PX b	  --源表
ON(a.id=b.class_id)   --条件
WHEN matched then UPDATE set a.BZR=b.name   --BZR修改字段 --name为源数据字段
WHEN not matched INSERT (a.id,b.BZR) VALUES (sys_guid,b.name)

//substr-instr截取数据
select 
	substr(----------------------------------------------截取返回str
    a.message_data,			
    instr(a.message_data,'returnOrderCode')+17,	---------截取返回num
    11 --------------------------------------------------此值为截取多少个文字
  )
from 
	message_track a
where
	rownum<10
	
//跨表修改数据
//1:表达方式一：
UPDATE b2c_order a 
SET a.is_o2o = '1' --修改成一样的值
WHERE
	a.CODE IN ( SELECT b.order_code FROM b2c_delivery b b.is_o2o = '1' )
//2:表达方式二：
UPDATE table_1 a 
SET a.NAME = ( SELECT b.NAME FROM table_2 b WHERE a.id = b.id ) --修改和表2一样的值
WHERE
	a.id IN ( SELECT b.id FROM table_2 b WHERE b.dr = 0 )
	
//触发器
create or replace TRIGGER "B2C_SALES_RATE_UPDATE" --触发器名
BEFORE UPDATE ON OCC_B2C.B2C_SALES_RATE ------------表名
	FOR EACH ROW------------------------------------每行触发
BEGIN 
  :NEW.TS:=SYSDATE;---------------------------------把新的TS字段更新为系统时间
END;

//oracle 循环插入数据
DECLARE
   strNum Varchar(30);
BEGIN
   for i in 1 .. 100 loop
		select 6902||substr(cast(dbms_random.value as varchar2(38)),3,9) into strNum from dual;
		INSERT into BASE_INTERNATIONAL_CODE_POOL (id,NATIVE_CODE,dr,ts,IS_USED )values(strNum,strNum,'0',sysdate(),'0');
   end loop;
   --commit;可以最后commit
END;
```



## 根据excel给的数据查询数据库内容

```sql
--表里有数据，先删数据 delete from b2c_order_temp_px
--建表
CREATE TABLE b2c_temp_px ( PX VARCHAR2 ( 40 ),PX_CODE VARCHAR2(40));
--插入数据
insert into b2c_temp_px (px) values ('DE20180907062319');
insert into b2c_temp_px (px) values ('DE20180907062542');
--查询
col 发货单号 format a20
col E3单号 format a20
col 中台单号 format a40

SELECT
	p.px 发货单号,
	d.E3_DELIVERY_CODE E3单号,
	d.PCODE 中台单号 
FROM
	B2C_DELIVERY d
	LEFT JOIN b2c_order_temp_px p ON d.code = p.PX 
WHERE
	1 = 1 
	AND d.CODE IN p.PX 
	AND ROWNUM < 20
```

## 数据库导入/导出

```sql
==数据库导出
1、ssh 127.0.0.1   --账户root/密码pwd
2、mkdir /home/oracle/binson  --linux创建目录
3、chmod -R 777 /home/oracle/binson/  --授权binson目录最高权限
4、su - oracle --进入oracle用户
5、sqlplus OLD_USER/passwd  --登陆 OLD_USER 用户
6、create or replace directory dump_dir as '/home/oracle/binson';  --创建逻辑目录
7、exit  --退出置 Oracle 账户下
8、sqlplus / as sysdba     --8步骤可以写成2步 1、sqlplus /nolog 2、conn /as sysdba 
9、grant read, write on directory dump_dir to OLD_USER;  --用sysdba用户授权 OLD_USER 用户权限
10、exit  --退出置 Oracle 账户下
--导出
11、expdp OLD_USER/passwd schemas=OLD_USER directory=dump_dir dumpfile=OLD_USER.2019xxxx.dmp logfile=OLD_USER.2019xxxx.log
12、或 exp OLD_USER/passwd file="/home/oracle/binson/OLD_USER.2019xxxx.dmp" full = y 
```

```sql
==数据库导入
1、sqlplus / as sysdba
--创建用户（这里省去了创建表空间，创建用户前需要创建表空间）
2、create user NEW_USER identified by passwd default tablespace BINSONDATA_01 temporary tablespace temp;
3、Grant connect,dba to NEW_USER;
--创建逻辑目录
4、create or replace directory dump_dir as '/home/oracle/binson'; 
5、grant read, write on directory dump_dir to NEW_USER; 
--导入
6、impdp NEW_USER/passwd directory=dump_dir dumpfile=OLD_USER.2019xxxx.dmp remap_schema=OLD_USER:NEW_USER
7、或imp NEW_USER/passwd BUFFER=64000 file=/home/oracle/binson/OLD_USER.2019xxxx.dmp FROMUSER=OLD_USER TOUSER=NEW_USER
```

# EntityManager:

## HQL/JPQL写法

```java
@PersistenceContext
private EntityManager entityManager;
public List<T> test(){
  String jpql="SELECT m FROM UserEntity m where m.id='1'";
  UserEntity userEntity=null;
  try {
       Query query = entityManager.createQuery(jpql.toString());
       userEntity =  query.getResultList();
  }catch (Exception e){
       e.printStackTrace();
  }finally {
       entityManager.close();
  }
}
```

## EntityManager的基础信息

```java
1、用@PersistenceContext动态注入
2、entitymanager的一些方法
persist() :添加实体Bean
flush() ：将实体的改变立刻刷新到数据库中
merge () :比较麻烦，用好了很不错，配合flush
Remove() :删除对象
createQuery() ：返回Query对象,以执行JPQL语句，（entity对象）
createNativeQuery() ：返回Query对象,以执行SQL语句,（原生sql）
refresh() ：刷新实体Bean,以得到对新对象
contains()： 检测实体当前是否被管理中
clear() 分离所有当前正在被管理的实体
```

## EntityManager分页用法

```java
//查询出所有的数据--1
    public List<TEntity> findAllData(Map<String, Object> searchParams, Pageable pageable) {
        String sql = sqlStr(searchParams, pageable);
        List<TEntity> entitys = new ArrayList<>();
        try {
            Query query = entityManager.createNativeQuery(sql.toString(), TEntity.class);
            //设置首条数据展示的位置
            query.setFirstResult(pageable.getPageNumber()*pageable.getPageSize());
            //设置多少页
            query.setMaxResults(pageable.getPageSize());
            entitys = query.getResultList();
            
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return entitys;
    }
```

```java
//分页查询--2
	public Map<String, Integer> findTotalElementsAndTotalPages(Map<String, Object> searchParams, Pageable pageable) {
        Map<String, Integer> map =new HashMap<>();
        List<Object> objectList=new ArrayList<>();
        Integer totalElements=0;
        String sql = sqlStr(searchParams, pageable);
    	//sql的拼装，查询有多少条数据
        String countSql = "select count(*) from (" + sql + ")";

        try {
            Query query = entityManager.createNativeQuery(countSql.toString());
            objectList=query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    	//获取到多少条数据
        if(objectList.size()!=0){
            totalElements= Integer.valueOf(objectList.get(0).toString());
        }else{
            totalElements=0;
        }
    	//设置多少条数据
        map.put("totalElements",totalElements);
    	//设置多少页--这里先把totalElements和pageable.getPageSize()转换为float数据，然后向上取整，然后再转换为int类型
        map.put("totalPages",(int)Math.ceil(((float)totalElements)/((float)pageable.getPageSize())));
        return  map;
    }
```

# Linux命令

```shell
一、查看Linux内核版本命令
1、cat /proc/version
2、uname -a

二、查看Linux系统版本信息的几种方法
1、lsb_release -a
2、cat /etc/redhat-release
3、cat /etc/issue
```

## CentOS 6.4 安装 Fcitx4.0

```
1、切换到root用户 su root
2、下载企鹅输入法 Fcitx4.0
3、tar zxvf fcitx-4.0.0_all.tar.gz
4、cd 解压目录
5、./configure //根据提示安装缺少文件，下面三个提醒错误
   //NO XRender Lib found： yum install tk-devel
   //NO Cairo-xlib found：yum install cairo-devel
   //NO PANGOCAIRO found：yum install pango-devel
6、make && make install
```



## Linux系统下无法 su - oracle

1、原因：是因为linux加载profile的顺序问题

2、解决方法：view . /bashrc    里的文件要写上“#exit”

## scp上传下载

```shell
==拉取文件从远程
scp 远程user@远程ip地址:/home/old_test.txt /home/binson/new_test.txt
==发送文件至远程
scp /home/binson/from_test.txt 远程user@远程ip地址:/home
```

## start_base.sh

```shell
#!/bin/sh
ps -ef | grep occ-base-web | grep -v grep | awk '{print $2}' | xargs kill -9
source="/home/data/build/source/ds-project-youngor-branch0910"
svn update ${source}/occ-base-api
svn update ${source}/occ-base-web
mvn clean install -f ${source}/occ-base-api
mvn clean install -o -f ${source}/occ-base-web
mv ${source}/occ-base-web/target/occ-base-web-1.0.0.jar /home/data/build/package
cd /home/data/build/package
nohup java  -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=2223 -jar /home/data/build/package/occ-base-web-1.0.0.jar --spring.profiles.active=test > /home/data/build/package/occ-base-web-1.0.0.log &
tail -f /home/data/build/package/occ-base-web-1.0.0.log
```

## pub_restart_single

```
#!/bin/bash
function re_start_app() {
    app=$1
    debug_port=$2
    if [ -f /data/jenkins/workspace/SourceCode/occ/$1/target/$1-1.0.0.jar ]; then
      ps -ef | grep $1 | grep -v grep | awk '{print $2}' | xargs  kill -9
 echo "执行的命令是:" "java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=$debug_port -jar /data/runtime/package/$1-1.0.0.jar --spring.profiles.active=test  > /data/runtime/package/$1-1.0.0.log"    
      echo killed [$app]
     \cp -f /data/jenkins/workspace/SourceCode/occ/$1/target/$1-1.0.0.jar /data/runtime/package/$1-1.0.0.jar
      java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=$debug_port -jar /data/runtime/package/$1-1.0.0.jar --spring.profiles.active=test  > /data/runtime/package/$1-1.0.0.log  &

      sleep 1 
      while :
      do
        n=$(cat /data/runtime/package/$1-1.0.0.log | grep Started)
        # echo ${n[0]} 
        if [[ ${n[0]} =~ "Started" ]]
        then
          echo ${n[0]}
          echo APP [$app]已启动
          break
        else
          echo 等待APP [$app] 启动...
          sleep 5
        fi 
      done
    else
       echo [$1]不存在
    fi
    return 1
}

#re_start_app occ-cmpt-web 1111

re_start_app occ-base-web 2222

#re_start_app occ-b2b-order-web 3333

#re_start_app occ-b2c-web 4444

#re_start_app occ-stock-web 5555

#re_start_app occ-settlement-web 6666

#re_start_app occ-book-web 7777  
```

## 前端编译代码

```shell
#!/bin/sh
code_base=/data/codes/ds-project-youngor
rm -fr $code_base/occ-portal-static/dist
svn update $code_base --username=wangfch --password=wangfch

#编译前端
cd $code_base/occ-portal-static
cnpm install
gulp install
\cp -fr $code_base/occ-portal-static/dist  $code_base/../compiled/
```

## 前端推送到服务器

```shell
#!/bin/sh
scp -r compiled/dist/*  192.168.88.198:/data/tomcat-ocm-web/webapps/ocm-web/
```

### RabbitMQ清空队列方法

```shell
进入到rabbit的安装路径，文件列表结构如下
进入到sbin目录
输入命令rabbitmqctl list_queues，有2万多条数据
关闭应用
rabbitmqctl stop_app
执行清除命令
rabbitmqctl reset
启动应用
rabbitmqctl start_app
验证清除结果rabbitmqctl list_queues
```

### 查询MQ库存积压  

```
1、cd rabbitmq_server-3.6.6/sbin/  
2、sh rabbitmqctl  list_queues				
```

# IDEA-SpringBoot启动指定文件

```
编辑启动配置->找到对应的启动项目->配置->程序参数->--spirng.profiles.active=gray
```

# JAVA基础

#### JAVA双亲委派模式及优势

```java
//机制:
当一个类被类加载器加载的时候,这个类会委托其父类加载器,若父类加载器还存在父类加载器,则进一步向上委托,依次递归,请求最终到达顶层启动类加载器,
如果父类加载器可以完成加载任务,就成功返回,倘若父类加载器无法完成加载,子类加载器才会自己去加载,这就是双亲委派模式.
//优势:
1:防止核心API的串改
2:避免类的重复加载
```



#### 事务

```java
//事务的原理：事务是利用aop实现的，事务的逻辑都是编织在代理对象上的
```

```java
@Service
public class ClassService {
	@Autowired
	NewClassService newClassService;
    
	@Transactional//默认 rollbackFor =Exception.class
	public void Test(User s) {
		newClassService.REQUIRES_NEW（s）;
	}
}

@Service
public class NewClassService {
	@Transactional(propagation=Propagation.REQUIRES_NEW)//新起事务
	public void REQUIRES_NEW(User s) {
		...
	}
}
```

```java
1、事务具有继承性，当继承性的事务（seervice里）抛出异常，那么整个外围都会回滚。
2、事务内有新起事务，这个事务具有隔离性，当这个新事务内部抓住异常了，外围事务不受影响。
3、一 、同一个类里，A和B两个方法，A调用B，A未加事务，B加事务；二、同一个类里，A调用B ，A加了事务，B加了新起事务 
		//这2种情况，前者A和B都无事务。后者A和B其实都是在一个事务里，B回滚A也会回滚，但是只有在不同的类里，B才能具有新事物，B回滚，A也可以不回滚，除非B抛出异常给A。
```

#### 数据库隔离性级别

```java
//Mysql默认设置为RR,Oracle默认设置为RC
（1）Read uncommitted(未授权读取、读未提交)：
   1）其他事务读未提交数据，出现脏读；
   2）如果一个事务已经开始写数据，则另外一个事务则不允许同时进行写操作，但允许其他事务读此行数据。该隔离级别可以通过“排他写锁”实现。
   3）避免了更新丢失，却可能出现脏读。也就是说事务B读取到了事务A未提交的数据。
    （读未提交：一个事务写数据时，只允许其他事务对这行数据进行读，所以会出现脏读，事务T1读取T2未提交的数据）

（2）Read committed（授权读取、读提交）：
   1）允许写事务，所以会出现不可重复读
   2）读取数据的事务允许其他事务继续访问该行数据，但是未提交的写事务将会禁止其他事务访问该行。
   3）该隔离级别避免了脏读，但是却可能出现不可重复读。事务A事先读取了数据，事务B紧接了更新了数据，并提交了事务，而事务A再次读取该数据时，数据已经发生了改变。
    （读已提交：读取数据的事务允许其他事务进行操作，避免了脏读，但是会出现不可重复读，事务T1读取数据，T2紧接着更新数据并提交数据，事务T1再次读取数据的时候，和第一次读的不一样。即虚读）

（3）Repeatable read（可重复读取）：
   1）禁止写事务；
   2）读取数据的事务将会禁止写事务（但允许读事务），写事务则禁止任何其他事务。
   3）避免了不可重复读取和脏读，但是有时可能出现幻读。这可以通过“共享读锁”和“排他写锁”实现。
    （可重复读：读事务会禁止所有的写事务，但是允许读事务，避免了不可重复读和脏读，但是会出现幻读，即第二次查询数据时会包含第一次查询中未出现的数据）

（4）Serializable（序列化）：
   1）禁止任何事务，一个一个进行；
   2）提供严格的事务隔离。它要求事务序列化执行，事务只能一个接着一个地执行，但不能并发执行。如果仅仅通过“行级锁”是无法实现事务序列化的，必须通过其他机制保证新插入的数据不会被刚执行查询操作的事务访问到。
   3）序列化是最高的事务隔离级别，同时代价也花费最高，性能很低，一般很少使用，在该级别下，事务顺序执行，不仅可以避免脏读、不可重复读，还避免了幻读。
```

#### instanceof

```
1:实现类和子类 instanceof 父类  	//true
2:实现类      instanceof 子类  	  //false
3:子类	   	instanceof 实现类	  //true

父类->实现类->子类
```



#### static和final

```java
/**
 * 静态的加载和执行顺序
 * 1 2 6 不打印输出
 */

public class Test {                             //1:第一步,准备加载类
    static int a=2;                             //2:第二步,静态变量和静态代码块的加载顺序由编写先后决定
    static {                                    //3:.第三步，静态块
        System.out.println("3");
    }
    public static void main(String[] args) {    //4:new一个类，但在new之前要处理匿名代码块
        System.out.println("4");
        new Test();
        Map<String,String> map=new HashMap<>();
    }
    {
        System.out.println("5");               //5:第五步，按照顺序加载匿名代码块，代码块中有打印
    }
    int b = 6;                                 //6.第六步，按照顺序加载变量
    Test() {
        System.out.println("7");               //7.第七步，最后加载构造函数，完成对象的建立
    }
    static void run(){
        System.out.println("静态方法，调用的时候才加载");
    }
}
```

```
1、final修饰的类，里面的方法和属性，都是final的，final类无法被继承，如：String；
2、static的方法是静态方法，所有的对象可以共享方法；
3、final方法的意义：
	第一、把方法锁定，防止任何继承类修改它的意义和实现。 
	第二、高效。编译器在遇到调用final方法时候会转入内嵌机制，大大提高执行效率
```



#### Lambda

```java
Map<Integer,String> map=new HashMap<>();
	map.put(1,"a");
    map.put(2,"b");
    map.put(3,"c");
    map.forEach((k,v)->{
    	if(k==3){
        	System.out.println(k+"_"+v);
        }
    });
//这里k、v为值，->后为执行方法，若只有一句，可以省略大括号
```

#### Map里拿取数据

```java
 Map<String, DTO> dtoMap=拿取到的数据塞入
 Set<String> keys=dtoMap.keySet();
 List<DTO> dtos=new ArrayList<>();
 for(String s:keys){
 	dtos.add(dtoMap.get(s));
 }
```

```java
 Map<String, DTO> dtoMap=拿取到的数据塞入 
 Set<String> keys=dtoMap.keySet();
 DTO s1=null;
 for(String x:keys){
     s1=dtoMap.get(x);
     DTO s2=service.xxx(s1.getId());
     if(s2!=null){
         dtoMap.remove(x);
     }
 }
```

#### 日期计算

```java
String dataStr="19901109";//日期
String day="8";//8天
SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
try {
    Date date = sdf.parse(dataStr);//解析为时间
    Long date2=date.getTime()-(Long.valueOf(day))*24*3600*1000;//往前8天
    String date3=sdf.format(date2);//结果为19901101
    System.out.print(date3);
} catch (ParseException e) {
    e.printStackTrace();
}
```

#### 右移一位:>>1

```java
这里是ArrayList源代码
int oldCapacity = elementData.length;
int newCapacity = oldCapacity + (oldCapacity >> 1)
解答：>>1是先将原来的数字转换为二进制，然后右移一位
	比如这里的oldCapacity=4，那么转换为二进制是100，右移一位变成10,10再转换为十进制就是2，那么newCapacity=6，相当于扩容1.5倍
```

#### 数据结构

Linked存快，链表结构。map是取的快，有hash值。

##### Array

```
1、长度固定
2、同一种数据类型的数据（Object可以多种类型）
3、添加数据和取数都比较快
```

##### List

###### 	ArrayList

```
1、基于动态数组
2、根据下表随机访问数组元素的效率高，向数组尾部添加元素的效率高
3、删除数组中的数据以及向数组中间添加数据效率低，因为需要移动数组
4、扩容大概为原先的1.5倍扩容
```

###### 	LinkedList

```
1、基于链表的动态数组
2、数据添加删除效率高，只需要改变指针指向
3、访问数据效率低，需要对链表进行遍历
```

##### Set

###### 	HashSet

```
1、是哈希表实现的，所以取数快，简版的HashMap
2、数据是无序的
3、每个元素必须是唯一的，就如数据库中唯一约束，可以放入null，但只能放入一个null
```

###### 	TreeSet

```
1、是二差树实现的
2、数据是自动排好序的
3、不允许放入null值。 
```

##### Map

###### 	HashMap

```
1、存储数据采用哈希表的结构，元素存取顺序不能保持一致
2、线程不安全，速度快。

//HashMap为什么这么快?
数组链表:便是定义一个数组, 然后数组的每一个成员都是一条链表,数组只需要记载这条链表的引用即可, 这样不需要直接在数组内部存储 键-值 对而需要大量的连续的内存空间;
散列机制:便是所谓的 hashCode 方法返回的 int 数(散列码), 他是通过对象的信息(默认是地址),通过某种散列的数学函数生成的一串 int 数字(散列码),散列码余上数组的容量,得到下标, 然后把 键-值对 存储在这个下标下对应的链表内;
PX理解:数组里的下标是根据散列码计算,根据散列码拿取到链表里的数据,链表里的数据则是根据key值来线性遍历,然后拿取到value值;
```

###### 	HashTable

```
1、线程安全，速度慢。
```

##### Collection和Map

```
1、Set 和List 都继承了Conllection；
2、Map没有继承于Collection接口，从Map集合中检索元素时，只要给出键对象，就会返回对应的值对象；
3、Collection、List、Set、Map都是接口，不能实例化。继承自它们的 ArrayList, Vector, HashTable, HashMap是具象class，这些才可被实例化；
```



# Nginx

```shell
作用：负载均衡，反向代理
正向代理：本身服务器处理请求，用户明确知道访问的是谁（典型的案例：翻墙）。
反向代理：本身不做请求处理，只是把请求分配给其他不同的服务器处理请求（典型的案例：负载均衡）。
文件对应：.zip=windows  tar.gz=Linux
```

```
Windows操作命令
一、启动 .../nginx/sbin/start nginx.exe
二、重启 .../nginx/sbin/nginx.exe -s reload         
三、停止 .../nginx/sbin/nginx.exe -s stop              
```

```
#负载均衡实际服务器地址
	upstream fuzaijunheng{
		server 127.0.0.1:8080 weight=3;
		server 127.0.0.1:8081 weight=1;
	}
    server {
        listen       80;
		#配置外网地址,可以修改电脑host地址
        server_name  localhost;
		
		#负载均衡地址
		#下面的/,为全匹配
		#fuzaijunheng，也可以直接是地址
		location / {
            proxy_pass http://fuzaijunheng;
            index  index.html index.htm;
        }
```



# 前端

##### 	绝对定位于相对定位

```html
<!-- 注：第一个div是相对定位，第二个div是绝对定位，第二个div定位是根据第一个定位来进行定位 -->
<div  style="position: relative;">
    <div style="position:absolute;top:-5px;right: 20px;"></div>
</div>

```

##### JS支持的六大类型+三大引用类型

```javascript
//5个基本类型+1个复杂数据类型
1、Number
2、String
3、Undefined
4、Boolean
5、Null
6、Object：
	var person = new Object();
	person.name = "Micheal";
	person.age = 24;
//3大引用类型
1、Object
2、Array
3、Function
```

