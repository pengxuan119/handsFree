package com.hs.hsbaseweb.dto;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
public class BrandDto  {

    private String id;

    private String name;

    private String code;
}
