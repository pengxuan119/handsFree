package com.hs.hsbaseweb.mapper;

import com.hs.hsbaseweb.dto.ProductDto;
import com.hs.hsbaseweb.entity.Brand;
import com.hs.hsbaseweb.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring",uses = {BrandMapper.class})
public interface ProductMapper  {

    @Mapping(source = "brand.id",target = "brandId")
    @Mapping(source = "brand.code",target = "brandCode")
    @Mapping(source = "brand.name",target = "brandName")
    public abstract ProductDto entityToDto(Product product);

}
