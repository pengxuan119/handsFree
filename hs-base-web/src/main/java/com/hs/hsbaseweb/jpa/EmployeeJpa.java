package com.hs.hsbaseweb.jpa;

import com.hs.hsbaseweb.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
@Repository
public interface EmployeeJpa extends JpaRepository<EmployeeEntity,String>,JpaSpecificationExecutor<EmployeeEntity>,Serializable {

}
