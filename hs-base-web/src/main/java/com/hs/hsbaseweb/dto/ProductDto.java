package com.hs.hsbaseweb.dto;

import com.hs.hsbaseweb.entity.Brand;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
public class ProductDto {

    private String id;

    private String name;

    private String code;

    private String brandId;
    private String brandCode;
    private String brandName;
}
