package com.hs.hsbaseweb.jpa;

import com.hs.hsbaseweb.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;

public interface UserJpa extends JpaRepository<UserEntity,String>,JpaSpecificationExecutor<UserEntity>,Serializable {

}
