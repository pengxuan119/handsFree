package com.hs.hsbaseweb.controller;

import com.hs.hsbaseweb.dto.ProductDto;
import com.hs.hsbaseweb.entity.Brand;
import com.hs.hsbaseweb.entity.Product;
import com.hs.hsbaseweb.jpa.BrandJpa;
import com.hs.hsbaseweb.jpa.ProductJpa;
import com.hs.hsbaseweb.mapper.ProductMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/base")
public class ProductController {
    @Autowired
    ProductJpa productJpa;
    @Autowired
    BrandJpa brandJpa;
    @Autowired
    ProductMapper productMapper;


    @RequestMapping(value = "/findAll",method = RequestMethod.GET)
    public List<ProductDto> findProduct(){

        Product product=productJpa.findById("123").get();
        ProductDto productDto= productMapper.entityToDto(product);

        List<ProductDto> productDtos=new ArrayList<>();
        productDtos.add(productDto);
        return productDtos;
    }

}
