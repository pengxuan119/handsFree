package com.hs.hsbaseweb.jpa;

import com.hs.hsbaseweb.entity.Brand;
import com.hs.hsbaseweb.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductJpa extends JpaRepository<Product,String> {
}
