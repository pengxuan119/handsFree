package com.hs.hsbaseweb.jpa;

import com.hs.hsbaseweb.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandJpa extends JpaRepository<Brand,String> {
}
