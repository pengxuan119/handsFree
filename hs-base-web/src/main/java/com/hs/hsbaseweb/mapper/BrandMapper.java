package com.hs.hsbaseweb.mapper;

import com.hs.hsbaseweb.dto.BrandDto;
import com.hs.hsbaseweb.entity.Brand;

public interface BrandMapper {
    public abstract BrandDto entityToDto(Brand brand);
}
